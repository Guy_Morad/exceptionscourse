#pragma once
#include <iostream>

#define INVALID_NUMBER 8200

int add(int a, int b) {
    int sum = a + b;
    if (sum == INVALID_NUMBER)
    {
        std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
        return 0;
    }
    return sum;
}

int  multiply(int a, int b) {
    int sum = 0;
    for (int i = 0; i < b; i++) {
        sum = add(sum, a);
    };

    if (sum == INVALID_NUMBER)
    {
        std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
        return 0;
    }

    return sum;
}

int  pow(int a, int b) {
    int exponent = 1;
    for (int i = 0; i < b; i++) {
        exponent = multiply(exponent, a);
    };

    if (exponent == INVALID_NUMBER)
    {
        std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
        return 0;
    }

    return exponent;
}

int main(void) {
    std::cout << pow(5, 5) << std::endl;
}
